<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'dashboard']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/register', [AuthController::class, 'registered']);
// Route::get('/welcome', [AuthController::class, 'welcome']);

Route::get('/table', function(){
    return view(('page.table'));
});

Route::get('/data-table', function(){
    return view('page.datatable');
});

// Route::get('/master', function(){
//     return view('layouts.master');
// });

// Create Data
// Route mengarah ke form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);
// Route untuk tambah data ke database
Route::post('/cast', [CastController::class, 'store']);

// Read Data
// untuk menampilkan semua kategori
Route::get('/cast', [CastController::class, 'index']);

// Route untuk detail cast berdasarkan id
Route::get('/cast/{id}', [CastController::class, 'show']);

// Update Data
// Route untuk mengarah ke form edit data dengan params id
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
// Route untuk update data di database berdasarkan id
Route::put('/cast/{id}', [CastController::class, 'update']);

// Delete Data
Route::delete('cast/{id}', [CastController::class, 'destroy']);
