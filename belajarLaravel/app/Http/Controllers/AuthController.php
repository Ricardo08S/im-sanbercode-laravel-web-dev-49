<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('form');
    }

    public function registered(Request $request)
    {
        $nama_depan = $request['fname'];
        $nama_belakang = $request['lname'];

        return view('welcome', ['nama_depan' => $nama_depan, 'nama_belakang' => $nama_belakang]);
    }

    // public function welcome()
    // {
    //     return view('welcome');
    // }

}